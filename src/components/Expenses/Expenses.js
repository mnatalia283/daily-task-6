import React, { useState } from 'react';
import ExpensesFilter from './ExpensesFilter';
import ExpensesChart from './ExpensesChart';
import ExpensesList from './ExpensesList';
import Card from '../UI/Card';
import './Expenses.css';


const Expenses = (props) => {
  const [filteredYear, setFilteredYear] = useState('2021')

  const filterChangeHandler = selectedYear => {
    setFilteredYear(selectedYear)
  }

  // buat original items
  let filteredExpense = props.items;
  if (filteredYear !== 'all') {
    filteredExpense = props.items.filter(item => {
      return (
        item.date.getFullYear() === parseInt(filteredYear)
      )
    })
  }

  // console.log(props.items)
  // const filteredExpense = props.items.filter(expense => {
  //   return expense.date.getFullYear().toString() === filteredYear
  // })

  return (
    <div>
      <Card className='expenses'>
        <ExpensesFilter selected={filteredYear} onChangeFilter={filterChangeHandler} />
        <ExpensesChart expenses={filteredExpense} />
        <ExpensesList items={filteredExpense} />
      </Card>
    </div>
  );
};


export default Expenses;
