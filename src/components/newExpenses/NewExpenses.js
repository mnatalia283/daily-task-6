import React, { useState } from "react";
import ExpensesForm from './ExpensesForm';
import './NewExpenses.css'

const NewExpenses = (props) => {
    const [isAddNewData, setIsAddNewData] = useState(false)
    const saveExpenseDataHandler = (EnteredExpensesData) => {
        const expenseData = {
            ...EnteredExpensesData,
            id: Math.random().toString()
        }
        // console.log(expenseData)
        props.onAddExpense(expenseData)
    }

    const startAddNewData = () => {
        setIsAddNewData(true);
    }

    const cancelAddNewData = () => {
        setIsAddNewData(false);
    }

    return (
        <div className='new-expense'>
            {!isAddNewData && <button onClick={startAddNewData}>add New Expenses</button>}
            {isAddNewData && <ExpensesForm onSaveExpensesData={saveExpenseDataHandler} onClick={cancelAddNewData} />}
        </div>
    )
}

export default NewExpenses;